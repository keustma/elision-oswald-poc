/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Sep 27, 2018 2:25:56 PM                     ---
 * ----------------------------------------------------------------
 */
package eu.elision.oswald.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedOswaldchatbotbackofficeConstants
{
	public static final String EXTENSIONNAME = "oswaldchatbotbackoffice";
	
	protected GeneratedOswaldchatbotbackofficeConstants()
	{
		// private constructor
	}
	
	
}
