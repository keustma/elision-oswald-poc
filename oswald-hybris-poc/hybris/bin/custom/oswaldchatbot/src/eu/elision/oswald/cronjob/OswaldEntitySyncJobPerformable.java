package eu.elision.oswald.cronjob;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedPropertyModel;
import eu.elision.oswald.dto.OswaldEntityResponseData;
import eu.elision.oswald.dto.OswaldSessionContext;
import eu.elision.oswald.model.OswaldCustomEntityValueModel;
import eu.elision.oswald.model.OswaldEntitySyncCronjobModel;
import eu.elision.oswald.service.OswaldService;
import eu.elision.oswald.strategy.EntityValueCollectorStrategy;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class OswaldEntitySyncJobPerformable extends AbstractJobPerformable<OswaldEntitySyncCronjobModel> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OswaldEntitySyncJobPerformable.class);

    private static final List<String> ENTITIES = Arrays.asList("brand", "brandName", "swatchColors", "size", "category", "categoryName", "price", "gender", "collectionName");

    @Resource(name = "solrEntityValueCollectorStrategy")
    private EntityValueCollectorStrategy solrEntityValueCollectorStrategy;

    @Resource
    private OswaldService oswaldService;

    @Override
    public PerformResult perform(final OswaldEntitySyncCronjobModel cronJobModel) {

        // TODO: Elke keer alle entities verwijderen en dan pas terug aanmaken om oude entities weg te krijgen ?

        if (StringUtils.isAnyEmpty(
                cronJobModel.getApiEndpoint(),
                cronJobModel.getChatbotId(),
                cronJobModel.getUsername(),
                cronJobModel.getPassword()
        )) {
            LOGGER.error("Can't start the export job without needed config settings!");
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
        }

        try {

            final OswaldSessionContext session = oswaldService.createSession(
                    cronJobModel.getApiEndpoint(),
                    cronJobModel.getChatbotId(),
                    cronJobModel.getUsername(),
                    cronJobModel.getPassword());

            final Set<SolrIndexedPropertyModel> solrIndexedProperties = cronJobModel.getSolrIndexedProperties();
            if (CollectionUtils.isNotEmpty(solrIndexedProperties)) {
                for (final SolrIndexedPropertyModel propertyModel : solrIndexedProperties) {
                    final String entity = propertyModel.getName();

                    try {
                        // TODO: Als er geen entityValues zijn maar wel een entity, deze leeg maken ?
                        final List<String> entityValues = solrEntityValueCollectorStrategy.collect(entity);

                        final String entityLabelId = findOrCreateEntityLabelId(session, entity);

                        for (final String entityValue : entityValues) {
                            oswaldService.createEntityValue(session, entityLabelId, entityValue);
                        }
                    } catch (final Exception ex) {
                        LOGGER.error("An error occured while pushing entity or values for entity {}", entity, ex);
                    }
                }
            }

            final Set<OswaldCustomEntityValueModel> customEntityValues = cronJobModel.getCustomEntityValue();
            if (CollectionUtils.isNotEmpty(customEntityValues)) {
                for (final OswaldCustomEntityValueModel customEntityValue : customEntityValues) {
                    final String entityLabelId = findOrCreateEntityLabelId(session, customEntityValue.getEntity());
                    oswaldService.createEntityValue(session, entityLabelId, customEntityValue.getValue());
                }
            }

            // Tests
            // final OswaldEntityResponseData[] entities = oswaldService.findEntities(session);

            // TODO: contains error: final OswaldEntityValueResponseData[] entityValues = oswaldService.findEntityValues(session);


            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        } catch (final Exception ex) {
            LOGGER.error("An error occured!", ex);
        }

        return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
    }

    private String findOrCreateEntityLabelId(final OswaldSessionContext session, final String entity) {
        final Optional<OswaldEntityResponseData> entityByLabel = oswaldService.findEntityByLabel(session, entity);

        // Only create the entity if we have values for it
        final String entityLabelId;
        if (entityByLabel.isPresent()) {
            entityLabelId = entityByLabel.get().getId();
        } else {
            final OswaldEntityResponseData createEntityResponse = oswaldService.createEntity(session, entity);
            entityLabelId = createEntityResponse.getId();
        }

        return entityLabelId;
    }
}
