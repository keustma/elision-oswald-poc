package eu.elision.oswald.strategy;

import java.util.List;

/**
 * @author mathkeus
 */
public interface EntityValueCollectorStrategy {

    List<String> collect(String entity);
}
