package eu.elision.oswald.dto;


import java.io.Serializable;
import java.util.List;

public class OswaldEntityValueResponseData implements Serializable {

    private String value;
    private List<String> synonyms;
    private boolean useForCorrections;
    private String id;
    private String chatbotId;
    private String labelId;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<String> getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(List<String> synonyms) {
        this.synonyms = synonyms;
    }

    public boolean isUseForCorrections() {
        return useForCorrections;
    }

    public void setUseForCorrections(boolean useForCorrections) {
        this.useForCorrections = useForCorrections;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChatbotId() {
        return chatbotId;
    }

    public void setChatbotId(String chatbotId) {
        this.chatbotId = chatbotId;
    }

    public String getLabelId() {
        return labelId;
    }

    public void setLabelId(String labelId) {
        this.labelId = labelId;
    }
}
