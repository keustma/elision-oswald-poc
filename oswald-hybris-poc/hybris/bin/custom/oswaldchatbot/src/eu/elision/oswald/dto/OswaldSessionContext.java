package eu.elision.oswald.dto;

public class OswaldSessionContext {

    private String chatbotId;
    private String apiEndpoint;
    private String accessToken;

    public String getChatbotId() {
        return chatbotId;
    }

    public void setChatbotId(final String chatbotId) {
        this.chatbotId = chatbotId;
    }

    public String getApiEndpoint() {
        return apiEndpoint;
    }

    public void setApiEndpoint(final String apiEndpoint) {
        this.apiEndpoint = apiEndpoint;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
    }
}
