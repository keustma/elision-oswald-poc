package eu.elision.oswald.service.impl;

import com.google.common.base.Preconditions;
import eu.elision.oswald.dto.*;
import eu.elision.oswald.service.OswaldService;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

public class OswaldServiceImpl implements OswaldService {

    @Override
    public OswaldSessionContext createSession(final String apiEndpoint, final String chatbotId, final String email, final String password) {
        Preconditions.checkNotNull(apiEndpoint);
        Preconditions.checkNotNull(chatbotId);
        Preconditions.checkNotNull(email);
        Preconditions.checkNotNull(password);

        final OswaldSessionContext session = new OswaldSessionContext();
        session.setApiEndpoint(apiEndpoint);
        session.setChatbotId(chatbotId);
        session.setAccessToken(loginAndReturnAccessToken(apiEndpoint, email, password));
        return session;
    }

    @Override
    public OswaldEntityResponseData createEntity(final OswaldSessionContext session, final String label) {
        Preconditions.checkNotNull(session);

        final ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
        final RestTemplate restTemplate = new RestTemplate(requestFactory);

        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        final OswaldEntityRequestData entityData = new OswaldEntityRequestData();
        entityData.setChatbotId(session.getChatbotId());
        entityData.setLabel(label);

        final UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromUriString(session.getApiEndpoint())
                .pathSegment("chatbots", session.getChatbotId(), "entity-labels")
                .queryParam("access_token", session.getAccessToken());

        final HttpEntity<OswaldEntityRequestData> request = new HttpEntity<>(entityData, headers);
        return restTemplate.postForObject(uriBuilder.toUriString(), request, OswaldEntityResponseData.class);
    }

    @Override
    public Optional<OswaldEntityResponseData> findEntityByLabel(final OswaldSessionContext session, final String label) {
        Preconditions.checkNotNull(session);

        final OswaldEntityResponseData[] entities = findEntities(session);

        if (entities == null) {
            return Optional.empty();
        }

        return Arrays.stream(entities)
                .filter(entity -> label.equals(entity.getLabel()))
                .findFirst();
    }

    @Override
    public OswaldEntityResponseData[] findEntities(final OswaldSessionContext session) {
        Preconditions.checkNotNull(session);

        final ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
        final RestTemplate restTemplate = new RestTemplate(requestFactory);

        final UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromUriString(session.getApiEndpoint())
                .pathSegment("chatbots", session.getChatbotId(), "entity-labels")
                .queryParam("access_token", session.getAccessToken());

        return restTemplate.getForObject(uriBuilder.toUriString(), OswaldEntityResponseData[].class);
    }

    @Override
    public OswaldEntityValueResponseData createEntityValue(final OswaldSessionContext session, final String entityId, final String entityValue) {
        Preconditions.checkNotNull(session);
        Preconditions.checkNotNull(entityId);
        Preconditions.checkNotNull(entityValue);

        // TODO clean up entity values (configurable value filters for each entity via backoffice ??)

        final ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
        final RestTemplate restTemplate = new RestTemplate(requestFactory);

        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        final OswaldEntityValueRequestData entityValueData = new OswaldEntityValueRequestData();
        entityValueData.setChatbotId(session.getChatbotId());
        entityValueData.setLabelId(entityId);
        entityValueData.setValue(entityValue);
        entityValueData.setUseForCorrections(true);

        final UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromUriString(session.getApiEndpoint())
                .pathSegment("chatbots", session.getChatbotId(), "entity-values")
                .queryParam("access_token", session.getAccessToken());

        final HttpEntity<OswaldEntityValueRequestData> request = new HttpEntity<>(entityValueData, headers);
        return restTemplate.postForObject(uriBuilder.toUriString(), request, OswaldEntityValueResponseData.class);
    }

    @Override
    public OswaldEntityValueResponseData[] findEntityValues(final OswaldSessionContext session) {
        Preconditions.checkNotNull(session);

        final ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
        final RestTemplate restTemplate = new RestTemplate(requestFactory);

        final UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromUriString(session.getApiEndpoint())
                .pathSegment("chatbots", session.getChatbotId(), "entity-values")
                .queryParam("access_token", session.getAccessToken());

        return restTemplate.getForObject(uriBuilder.toUriString(), OswaldEntityValueResponseData[].class);
    }

    private String loginAndReturnAccessToken(final String apiEndpoint, final String email, final String password) {
        Preconditions.checkNotNull(apiEndpoint);
        Preconditions.checkNotNull(email);
        Preconditions.checkNotNull(password);

        final ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
        final RestTemplate restTemplate = new RestTemplate(requestFactory);

        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        final OswaldUserLoginRequestData userLoginData = new OswaldUserLoginRequestData();
        userLoginData.setEmail(email);
        userLoginData.setPassword(password);

        final UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromUriString(apiEndpoint)
                .pathSegment("users", "login");

        final HttpEntity<OswaldUserLoginRequestData> request = new HttpEntity<>(userLoginData, headers);
        final OswaldUserLoginResponseData response = restTemplate.postForObject(uriBuilder.toUriString(), request, OswaldUserLoginResponseData.class);

        return response.getId();
    }

    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        final int timeout = 5000;
        final RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout)
                .setConnectionRequestTimeout(timeout)
                .setSocketTimeout(timeout)
                .build();
        final CloseableHttpClient client = HttpClientBuilder
                .create()
                .setDefaultRequestConfig(config)
                .build();
        return new HttpComponentsClientHttpRequestFactory(client);
    }
}
