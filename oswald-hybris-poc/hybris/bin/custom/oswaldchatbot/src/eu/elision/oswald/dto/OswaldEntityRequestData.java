package eu.elision.oswald.dto;

import java.io.Serializable;

public class OswaldEntityRequestData implements Serializable {

    private String chatbotId;
    private String label;

    public String getChatbotId() {
        return chatbotId;
    }

    public void setChatbotId(String chatbotId) {
        this.chatbotId = chatbotId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}

