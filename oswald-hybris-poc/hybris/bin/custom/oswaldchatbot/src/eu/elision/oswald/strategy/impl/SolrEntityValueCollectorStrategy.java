package eu.elision.oswald.strategy.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.SolrFacetSearchConfigSelectionStrategy;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.exceptions.NoValidSolrConfigException;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.search.*;
import eu.elision.oswald.strategy.EntityValueCollectorStrategy;
import org.apache.commons.collections4.CollectionUtils;
import org.assertj.core.util.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author mathkeus
 */
public class SolrEntityValueCollectorStrategy implements EntityValueCollectorStrategy {

    private static final Logger LOGGER = LoggerFactory.getLogger(SolrEntityValueCollectorStrategy.class);

    @Resource
    private FacetSearchConfigService facetSearchConfigService;

    @Resource(name = "defaultSolrFacetSearchConfigSelectionStrategy")
    private SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy;

    @Resource
    private CatalogVersionService catalogVersionService;

    @Resource
    private FacetSearchService solrFacetSearchService;

    @Override
    public List<String> collect(String entity) {

        try {
            FacetSearchConfig facetSearchConfig = getFacetSearchConfig();
            final SearchQuery searchQuery = solrFacetSearchService.createSearchQuery(facetSearchConfig, getIndexedType(facetSearchConfig));

            searchQuery.setCatalogVersions(Collections.singletonList(catalogVersionService.getCatalogVersion("apparelProductCatalog", "Online")));
            searchQuery.setCurrency("EUR");
            searchQuery.setLanguage("en");
            searchQuery.setPageSize(0);
            searchQuery.setEnableSpellcheck(true);
            searchQuery.addFacet(entity);

            final SearchResult searchResult = solrFacetSearchService.search(searchQuery);
            final Facet facet = searchResult.getFacet(entity);

            return facet.getFacetValueNames();
        } catch (final NoValidSolrConfigException e) {
            LOGGER.error("No valid solrFacetSearchConfig found for the current context", e);
        } catch (final FacetConfigServiceException | FacetSearchException e) {
            LOGGER.error(e.getMessage(), e);
        }


        return Lists.emptyList();
    }

    private FacetSearchConfig getFacetSearchConfig() throws NoValidSolrConfigException, FacetConfigServiceException {
        final SolrFacetSearchConfigModel solrFacetSearchConfigModel = solrFacetSearchConfigSelectionStrategy
                .getCurrentSolrFacetSearchConfig();
        return facetSearchConfigService.getConfiguration(solrFacetSearchConfigModel.getName());
    }

    private IndexedType getIndexedType(final FacetSearchConfig config) {
        final IndexConfig indexConfig = config.getIndexConfig();

        // Strategy for working out which of the available indexed types to use
        final Collection<IndexedType> indexedTypes = indexConfig.getIndexedTypes().values();
        if (CollectionUtils.isNotEmpty(indexedTypes)) {
            // When there are multiple - select the first
            return indexedTypes.iterator().next();
        }

        // No indexed types
        return null;
    }
}
