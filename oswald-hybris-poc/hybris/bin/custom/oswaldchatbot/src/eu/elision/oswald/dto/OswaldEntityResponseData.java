package eu.elision.oswald.dto;

import java.io.Serializable;

public class OswaldEntityResponseData implements Serializable {

    private String label;
    private String id;
    private String chatbotId;
    private boolean useForCorrections;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChatbotId() {
        return chatbotId;
    }

    public void setChatbotId(String chatbotId) {
        this.chatbotId = chatbotId;
    }

    public boolean isUseForCorrections() {
        return useForCorrections;
    }

    public void setUseForCorrections(boolean useForCorrections) {
        this.useForCorrections = useForCorrections;
    }
}
