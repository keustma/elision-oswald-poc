package eu.elision.oswald.dto;

import java.io.Serializable;

public class OswaldEntityValueRequestData implements Serializable {

    private boolean useForCorrections;
    private String[] synonyms;
    private String chatbotId;
    private String labelId;
    private String value;

    public boolean isUseForCorrections() {
        return useForCorrections;
    }

    public void setUseForCorrections(boolean useForCorrections) {
        this.useForCorrections = useForCorrections;
    }

    public String[] getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(String[] synonyms) {
        this.synonyms = synonyms;
    }

    public String getChatbotId() {
        return chatbotId;
    }

    public void setChatbotId(String chatbotId) {
        this.chatbotId = chatbotId;
    }

    public String getLabelId() {
        return labelId;
    }

    public void setLabelId(String labelId) {
        this.labelId = labelId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

