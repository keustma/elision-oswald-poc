package eu.elision.oswald.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import eu.elision.oswald.constants.OswaldchatbotConstants;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class OswaldchatbotManager extends GeneratedOswaldchatbotManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( OswaldchatbotManager.class.getName() );
	
	public static final OswaldchatbotManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (OswaldchatbotManager) em.getExtension(OswaldchatbotConstants.EXTENSIONNAME);
	}
	
}
