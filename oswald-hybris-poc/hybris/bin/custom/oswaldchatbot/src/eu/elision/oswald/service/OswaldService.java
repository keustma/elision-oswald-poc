package eu.elision.oswald.service;

import eu.elision.oswald.dto.OswaldEntityResponseData;
import eu.elision.oswald.dto.OswaldEntityValueResponseData;
import eu.elision.oswald.dto.OswaldSessionContext;

import java.util.Optional;

public interface OswaldService {

    OswaldSessionContext createSession(String apiEndpoint, String chatbotId, String email, String password);

    OswaldEntityResponseData createEntity(OswaldSessionContext session, String label);

    OswaldEntityResponseData[] findEntities(OswaldSessionContext session);

    Optional<OswaldEntityResponseData> findEntityByLabel(OswaldSessionContext session, String label);

    OswaldEntityValueResponseData createEntityValue(OswaldSessionContext session, String entityId, String value);

    OswaldEntityValueResponseData[] findEntityValues(OswaldSessionContext session);
}
