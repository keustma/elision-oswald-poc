/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Sep 27, 2018 4:08:32 PM                     ---
 * ----------------------------------------------------------------
 */
package eu.elision.oswald.jalo;

import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import eu.elision.oswald.constants.OswaldchatbotConstants;
import eu.elision.oswald.jalo.OswaldEntitySyncCronjob;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.jalo.GenericItem OswaldCustomEntityValue}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedOswaldCustomEntityValue extends GenericItem
{
	/** Qualifier of the <code>OswaldCustomEntityValue.entity</code> attribute **/
	public static final String ENTITY = "entity";
	/** Qualifier of the <code>OswaldCustomEntityValue.value</code> attribute **/
	public static final String VALUE = "value";
	/** Qualifier of the <code>OswaldCustomEntityValue.oswaldEntitySyncCronjob</code> attribute **/
	public static final String OSWALDENTITYSYNCCRONJOB = "oswaldEntitySyncCronjob";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n OSWALDENTITYSYNCCRONJOB's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedOswaldCustomEntityValue> OSWALDENTITYSYNCCRONJOBHANDLER = new BidirectionalOneToManyHandler<GeneratedOswaldCustomEntityValue>(
	OswaldchatbotConstants.TC.OSWALDCUSTOMENTITYVALUE,
	false,
	"oswaldEntitySyncCronjob",
	null,
	false,
	true,
	CollectionType.SET
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(ENTITY, AttributeMode.INITIAL);
		tmp.put(VALUE, AttributeMode.INITIAL);
		tmp.put(OSWALDENTITYSYNCCRONJOB, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		OSWALDENTITYSYNCCRONJOBHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldCustomEntityValue.entity</code> attribute.
	 * @return the entity
	 */
	public String getEntity(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENTITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldCustomEntityValue.entity</code> attribute.
	 * @return the entity
	 */
	public String getEntity()
	{
		return getEntity( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldCustomEntityValue.entity</code> attribute. 
	 * @param value the entity
	 */
	public void setEntity(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENTITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldCustomEntityValue.entity</code> attribute. 
	 * @param value the entity
	 */
	public void setEntity(final String value)
	{
		setEntity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldCustomEntityValue.oswaldEntitySyncCronjob</code> attribute.
	 * @return the oswaldEntitySyncCronjob
	 */
	public OswaldEntitySyncCronjob getOswaldEntitySyncCronjob(final SessionContext ctx)
	{
		return (OswaldEntitySyncCronjob)getProperty( ctx, OSWALDENTITYSYNCCRONJOB);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldCustomEntityValue.oswaldEntitySyncCronjob</code> attribute.
	 * @return the oswaldEntitySyncCronjob
	 */
	public OswaldEntitySyncCronjob getOswaldEntitySyncCronjob()
	{
		return getOswaldEntitySyncCronjob( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldCustomEntityValue.oswaldEntitySyncCronjob</code> attribute. 
	 * @param value the oswaldEntitySyncCronjob
	 */
	public void setOswaldEntitySyncCronjob(final SessionContext ctx, final OswaldEntitySyncCronjob value)
	{
		OSWALDENTITYSYNCCRONJOBHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldCustomEntityValue.oswaldEntitySyncCronjob</code> attribute. 
	 * @param value the oswaldEntitySyncCronjob
	 */
	public void setOswaldEntitySyncCronjob(final OswaldEntitySyncCronjob value)
	{
		setOswaldEntitySyncCronjob( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldCustomEntityValue.value</code> attribute.
	 * @return the value
	 */
	public String getValue(final SessionContext ctx)
	{
		return (String)getProperty( ctx, VALUE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldCustomEntityValue.value</code> attribute.
	 * @return the value
	 */
	public String getValue()
	{
		return getValue( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldCustomEntityValue.value</code> attribute. 
	 * @param value the value
	 */
	public void setValue(final SessionContext ctx, final String value)
	{
		setProperty(ctx, VALUE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldCustomEntityValue.value</code> attribute. 
	 * @param value the value
	 */
	public void setValue(final String value)
	{
		setValue( getSession().getSessionContext(), value );
	}
	
}
