/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Sep 27, 2018 4:08:32 PM                     ---
 * ----------------------------------------------------------------
 */
package eu.elision.oswald.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedOswaldchatbotConstants
{
	public static final String EXTENSIONNAME = "oswaldchatbot";
	public static class TC
	{
		public static final String OSWALDCUSTOMENTITYVALUE = "OswaldCustomEntityValue".intern();
		public static final String OSWALDENTITYSYNCCRONJOB = "OswaldEntitySyncCronjob".intern();
	}
	public static class Attributes
	{
		public static class SolrIndexedProperty
		{
			public static final String OSWALDENTITYSYNCCRONJOB = "oswaldEntitySyncCronjob".intern();
		}
	}
	public static class Relations
	{
		public static final String OSWALDENTITYSYNCJOB2CUSTOMENTITYVALUE = "OswaldEntitySyncJob2CustomEntityValue".intern();
		public static final String OSWALDENTITYSYNCJOB2SOLRINDEXEDPROP = "OswaldEntitySyncJob2SolrIndexedProp".intern();
	}
	
	protected GeneratedOswaldchatbotConstants()
	{
		// private constructor
	}
	
	
}
