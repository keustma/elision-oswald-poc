/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Sep 27, 2018 4:08:32 PM                     ---
 * ----------------------------------------------------------------
 */
package eu.elision.oswald.jalo;

import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.solrfacetsearch.jalo.config.SolrIndexedProperty;
import eu.elision.oswald.constants.OswaldchatbotConstants;
import eu.elision.oswald.jalo.OswaldCustomEntityValue;
import eu.elision.oswald.jalo.OswaldEntitySyncCronjob;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type <code>OswaldchatbotManager</code>.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedOswaldchatbotManager extends Extension
{
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put("oswaldEntitySyncCronjob", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.solrfacetsearch.jalo.config.SolrIndexedProperty", Collections.unmodifiableMap(tmp));
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	public OswaldCustomEntityValue createOswaldCustomEntityValue(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( OswaldchatbotConstants.TC.OSWALDCUSTOMENTITYVALUE );
			return (OswaldCustomEntityValue)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating OswaldCustomEntityValue : "+e.getMessage(), 0 );
		}
	}
	
	public OswaldCustomEntityValue createOswaldCustomEntityValue(final Map attributeValues)
	{
		return createOswaldCustomEntityValue( getSession().getSessionContext(), attributeValues );
	}
	
	public OswaldEntitySyncCronjob createOswaldEntitySyncCronjob(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( OswaldchatbotConstants.TC.OSWALDENTITYSYNCCRONJOB );
			return (OswaldEntitySyncCronjob)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating OswaldEntitySyncCronjob : "+e.getMessage(), 0 );
		}
	}
	
	public OswaldEntitySyncCronjob createOswaldEntitySyncCronjob(final Map attributeValues)
	{
		return createOswaldEntitySyncCronjob( getSession().getSessionContext(), attributeValues );
	}
	
	@Override
	public String getName()
	{
		return OswaldchatbotConstants.EXTENSIONNAME;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SolrIndexedProperty.oswaldEntitySyncCronjob</code> attribute.
	 * @return the oswaldEntitySyncCronjob
	 */
	public OswaldEntitySyncCronjob getOswaldEntitySyncCronjob(final SessionContext ctx, final SolrIndexedProperty item)
	{
		return (OswaldEntitySyncCronjob)item.getProperty( ctx, OswaldchatbotConstants.Attributes.SolrIndexedProperty.OSWALDENTITYSYNCCRONJOB);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SolrIndexedProperty.oswaldEntitySyncCronjob</code> attribute.
	 * @return the oswaldEntitySyncCronjob
	 */
	public OswaldEntitySyncCronjob getOswaldEntitySyncCronjob(final SolrIndexedProperty item)
	{
		return getOswaldEntitySyncCronjob( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SolrIndexedProperty.oswaldEntitySyncCronjob</code> attribute. 
	 * @param value the oswaldEntitySyncCronjob
	 */
	public void setOswaldEntitySyncCronjob(final SessionContext ctx, final SolrIndexedProperty item, final OswaldEntitySyncCronjob value)
	{
		item.setProperty(ctx, OswaldchatbotConstants.Attributes.SolrIndexedProperty.OSWALDENTITYSYNCCRONJOB,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SolrIndexedProperty.oswaldEntitySyncCronjob</code> attribute. 
	 * @param value the oswaldEntitySyncCronjob
	 */
	public void setOswaldEntitySyncCronjob(final SolrIndexedProperty item, final OswaldEntitySyncCronjob value)
	{
		setOswaldEntitySyncCronjob( getSession().getSessionContext(), item, value );
	}
	
}
