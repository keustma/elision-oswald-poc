/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Sep 27, 2018 4:08:32 PM                     ---
 * ----------------------------------------------------------------
 */
package eu.elision.oswald.jalo;

import de.hybris.platform.cronjob.jalo.CronJob;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.solrfacetsearch.constants.SolrfacetsearchConstants;
import de.hybris.platform.solrfacetsearch.jalo.config.SolrIndexedProperty;
import de.hybris.platform.util.OneToManyHandler;
import eu.elision.oswald.constants.OswaldchatbotConstants;
import eu.elision.oswald.jalo.OswaldCustomEntityValue;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Generated class for type {@link de.hybris.platform.cronjob.jalo.CronJob OswaldEntitySyncCronjob}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedOswaldEntitySyncCronjob extends CronJob
{
	/** Qualifier of the <code>OswaldEntitySyncCronjob.apiEndpoint</code> attribute **/
	public static final String APIENDPOINT = "apiEndpoint";
	/** Qualifier of the <code>OswaldEntitySyncCronjob.chatbotId</code> attribute **/
	public static final String CHATBOTID = "chatbotId";
	/** Qualifier of the <code>OswaldEntitySyncCronjob.username</code> attribute **/
	public static final String USERNAME = "username";
	/** Qualifier of the <code>OswaldEntitySyncCronjob.password</code> attribute **/
	public static final String PASSWORD = "password";
	/** Qualifier of the <code>OswaldEntitySyncCronjob.solrIndexedProperties</code> attribute **/
	public static final String SOLRINDEXEDPROPERTIES = "solrIndexedProperties";
	/** Qualifier of the <code>OswaldEntitySyncCronjob.customEntityValue</code> attribute **/
	public static final String CUSTOMENTITYVALUE = "customEntityValue";
	/**
	* {@link OneToManyHandler} for handling 1:n SOLRINDEXEDPROPERTIES's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<SolrIndexedProperty> SOLRINDEXEDPROPERTIESHANDLER = new OneToManyHandler<SolrIndexedProperty>(
	SolrfacetsearchConstants.TC.SOLRINDEXEDPROPERTY,
	false,
	"oswaldEntitySyncCronjob",
	null,
	false,
	true,
	CollectionType.SET
	);
	/**
	* {@link OneToManyHandler} for handling 1:n CUSTOMENTITYVALUE's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<OswaldCustomEntityValue> CUSTOMENTITYVALUEHANDLER = new OneToManyHandler<OswaldCustomEntityValue>(
	OswaldchatbotConstants.TC.OSWALDCUSTOMENTITYVALUE,
	false,
	"oswaldEntitySyncCronjob",
	null,
	false,
	true,
	CollectionType.SET
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(CronJob.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(APIENDPOINT, AttributeMode.INITIAL);
		tmp.put(CHATBOTID, AttributeMode.INITIAL);
		tmp.put(USERNAME, AttributeMode.INITIAL);
		tmp.put(PASSWORD, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldEntitySyncCronjob.apiEndpoint</code> attribute.
	 * @return the apiEndpoint
	 */
	public String getApiEndpoint(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APIENDPOINT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldEntitySyncCronjob.apiEndpoint</code> attribute.
	 * @return the apiEndpoint
	 */
	public String getApiEndpoint()
	{
		return getApiEndpoint( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldEntitySyncCronjob.apiEndpoint</code> attribute. 
	 * @param value the apiEndpoint
	 */
	public void setApiEndpoint(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APIENDPOINT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldEntitySyncCronjob.apiEndpoint</code> attribute. 
	 * @param value the apiEndpoint
	 */
	public void setApiEndpoint(final String value)
	{
		setApiEndpoint( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldEntitySyncCronjob.chatbotId</code> attribute.
	 * @return the chatbotId
	 */
	public String getChatbotId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CHATBOTID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldEntitySyncCronjob.chatbotId</code> attribute.
	 * @return the chatbotId
	 */
	public String getChatbotId()
	{
		return getChatbotId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldEntitySyncCronjob.chatbotId</code> attribute. 
	 * @param value the chatbotId
	 */
	public void setChatbotId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CHATBOTID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldEntitySyncCronjob.chatbotId</code> attribute. 
	 * @param value the chatbotId
	 */
	public void setChatbotId(final String value)
	{
		setChatbotId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldEntitySyncCronjob.customEntityValue</code> attribute.
	 * @return the customEntityValue
	 */
	public Set<OswaldCustomEntityValue> getCustomEntityValue(final SessionContext ctx)
	{
		return (Set<OswaldCustomEntityValue>)CUSTOMENTITYVALUEHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldEntitySyncCronjob.customEntityValue</code> attribute.
	 * @return the customEntityValue
	 */
	public Set<OswaldCustomEntityValue> getCustomEntityValue()
	{
		return getCustomEntityValue( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldEntitySyncCronjob.customEntityValue</code> attribute. 
	 * @param value the customEntityValue
	 */
	public void setCustomEntityValue(final SessionContext ctx, final Set<OswaldCustomEntityValue> value)
	{
		CUSTOMENTITYVALUEHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldEntitySyncCronjob.customEntityValue</code> attribute. 
	 * @param value the customEntityValue
	 */
	public void setCustomEntityValue(final Set<OswaldCustomEntityValue> value)
	{
		setCustomEntityValue( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to customEntityValue. 
	 * @param value the item to add to customEntityValue
	 */
	public void addToCustomEntityValue(final SessionContext ctx, final OswaldCustomEntityValue value)
	{
		CUSTOMENTITYVALUEHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to customEntityValue. 
	 * @param value the item to add to customEntityValue
	 */
	public void addToCustomEntityValue(final OswaldCustomEntityValue value)
	{
		addToCustomEntityValue( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from customEntityValue. 
	 * @param value the item to remove from customEntityValue
	 */
	public void removeFromCustomEntityValue(final SessionContext ctx, final OswaldCustomEntityValue value)
	{
		CUSTOMENTITYVALUEHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from customEntityValue. 
	 * @param value the item to remove from customEntityValue
	 */
	public void removeFromCustomEntityValue(final OswaldCustomEntityValue value)
	{
		removeFromCustomEntityValue( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldEntitySyncCronjob.password</code> attribute.
	 * @return the password
	 */
	public String getPassword(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PASSWORD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldEntitySyncCronjob.password</code> attribute.
	 * @return the password
	 */
	public String getPassword()
	{
		return getPassword( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldEntitySyncCronjob.password</code> attribute. 
	 * @param value the password
	 */
	public void setPassword(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PASSWORD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldEntitySyncCronjob.password</code> attribute. 
	 * @param value the password
	 */
	public void setPassword(final String value)
	{
		setPassword( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldEntitySyncCronjob.solrIndexedProperties</code> attribute.
	 * @return the solrIndexedProperties
	 */
	public Set<SolrIndexedProperty> getSolrIndexedProperties(final SessionContext ctx)
	{
		return (Set<SolrIndexedProperty>)SOLRINDEXEDPROPERTIESHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldEntitySyncCronjob.solrIndexedProperties</code> attribute.
	 * @return the solrIndexedProperties
	 */
	public Set<SolrIndexedProperty> getSolrIndexedProperties()
	{
		return getSolrIndexedProperties( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldEntitySyncCronjob.solrIndexedProperties</code> attribute. 
	 * @param value the solrIndexedProperties
	 */
	public void setSolrIndexedProperties(final SessionContext ctx, final Set<SolrIndexedProperty> value)
	{
		SOLRINDEXEDPROPERTIESHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldEntitySyncCronjob.solrIndexedProperties</code> attribute. 
	 * @param value the solrIndexedProperties
	 */
	public void setSolrIndexedProperties(final Set<SolrIndexedProperty> value)
	{
		setSolrIndexedProperties( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to solrIndexedProperties. 
	 * @param value the item to add to solrIndexedProperties
	 */
	public void addToSolrIndexedProperties(final SessionContext ctx, final SolrIndexedProperty value)
	{
		SOLRINDEXEDPROPERTIESHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to solrIndexedProperties. 
	 * @param value the item to add to solrIndexedProperties
	 */
	public void addToSolrIndexedProperties(final SolrIndexedProperty value)
	{
		addToSolrIndexedProperties( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from solrIndexedProperties. 
	 * @param value the item to remove from solrIndexedProperties
	 */
	public void removeFromSolrIndexedProperties(final SessionContext ctx, final SolrIndexedProperty value)
	{
		SOLRINDEXEDPROPERTIESHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from solrIndexedProperties. 
	 * @param value the item to remove from solrIndexedProperties
	 */
	public void removeFromSolrIndexedProperties(final SolrIndexedProperty value)
	{
		removeFromSolrIndexedProperties( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldEntitySyncCronjob.username</code> attribute.
	 * @return the username
	 */
	public String getUsername(final SessionContext ctx)
	{
		return (String)getProperty( ctx, USERNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>OswaldEntitySyncCronjob.username</code> attribute.
	 * @return the username
	 */
	public String getUsername()
	{
		return getUsername( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldEntitySyncCronjob.username</code> attribute. 
	 * @param value the username
	 */
	public void setUsername(final SessionContext ctx, final String value)
	{
		setProperty(ctx, USERNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>OswaldEntitySyncCronjob.username</code> attribute. 
	 * @param value the username
	 */
	public void setUsername(final String value)
	{
		setUsername( getSession().getSessionContext(), value );
	}
	
}
